/*
 * WLIP.cpp
 *
 *  Created on: Sep 16, 2019
 *      Author: pioann
 */

#include "WLIP.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

WLIP::WLIP() {
	this->messageString = NULL;
}

WLIP::~WLIP() {

}

void subArrayCopy(uint8_t *src, uint8_t *dest, uint8_t from, uint8_t n) {
    memcpy(dest, &src[from], sizeof(int)*n);
}

void WLIP::build(uint8_t *m) {
	this->setMessageString(m);

	this->splitMessage(); // FIXME: memory leak here?
}

const Message& WLIP::getMessage() const {
	return message;
}

uint8_t* WLIP::getMessageString() const {
	return messageString;
}

void WLIP::setMessageString(uint8_t *messageString) {
	this->messageString = messageString;
}

void WLIP::setMessage(const Message &message) {
	this->message = message;
}

void WLIP::splitMessage() {
	// get the command
	this->message.mCommand = this->messageString[0];

	// get the header
	memcpy(this->message.mHeader, &this->messageString[1], 3 * sizeof(uint8_t));
	this->parseHeader();
	// subArrayCopy(this->messageString, this->message.mHeader, 1, 3);



	memset(this->data.mDataHeader, 0, 20);
	uint8_t dataHeaderSize = this->header.dataHeaderLength;
	uint8_t dataHeaderPos = 1 + 3; // command + header length
	for (int i=0; i<dataHeaderSize; i++) {
		this->data.mDataHeader[i] = this->messageString[dataHeaderPos+i];
	}

	memset(this->data.mPayload, 0, 200);
	// last byte string termination. -2 is the CRC16-CCITT
	uint8_t dataSize = this->header.packetLength - (this->header.dataHeaderLength + 3 + 1 ) + 1 -2;

	uint8_t dataPos = 1 + 3 + this->header.dataHeaderLength; // command + header length + dataHeaderLength
	for (int i=0; i<dataSize; i++) {
		this->data.mPayload[i] = this->messageString[dataPos+i];
	}

	// get the dataHeader
	/*uint8_t dataHeaderSize = this->header.dataHeaderLength;
	uint8_t dataHeaderPos = 1 + 3; // command + header length
	this->data.mDataHeader = (uint8_t*)malloc(dataHeaderSize);
	memcpy(this->data.mDataHeader, &this->messageString[dataHeaderPos], dataHeaderSize);
	this->parseDataHeader();*/

	// get the payload
	/*uint8_t dataSize = this->header.packetLength - (this->header.dataHeaderLength + 3 + 1);
	uint8_t dataPos = 1 + 3 + this->header.dataHeaderLength; // command + header length + dataHeaderLength
	this->message.mData.mPayload = (uint8_t*)malloc(dataSize*sizeof(uint8_t));
	memcpy(this->message.mData.mPayload, &this->messageString[dataPos], dataSize * sizeof(uint8_t));*/
	// this->parsePayload(); // FIXME: MEMORY LEAK HERE! TODO: FIX!
}

void WLIP::parseHeader() {
	this->header.packetLength = this->message.mHeader[0];

	this->header.dataControl.mode = this->message.mHeader[1] & 0x0F;

	this->header.setupInfo = this->message.mHeader[2];

	this->header.sourceID = this->header.setupInfo >> 5; // get top 3 bits (MSB)
	this->header.dataHeaderLength = this->header.setupInfo & 0x1F; // get the first 5 bits (LSB)
}

void WLIP::parseDataHeader() {
	// this->data.mDataHeader = this->message.mData.mDataHeader;
}

/**
 * Check CRC-16 XMODEM. polynomial: 0x1021
 */
bool WLIP::isFrameValid(unsigned int length) {
	unsigned int i;
    unsigned short crc = 0;

    for(i=0; i<length-2; i++){
		crc = (unsigned char)(crc >>8) | (crc<<8);
		crc ^= this->messageString[i];
		crc ^= (unsigned char)(crc & 0xff) >> 4;
		crc ^= crc << 12;
		crc ^= (crc & 0x00ff) << 5;
    }
    // reverse the bytes
    unsigned char b = ((char*) &crc)[0];
	unsigned char a = ((char*) &crc)[1];

	if (this->messageString[length-2] == a && this->messageString[length-1] == b) {
		return true;
	}

	return false;
}

bool WLIP::isFrameValidTest(unsigned int length) {
	unsigned int i;
    unsigned short crc = 0;

    for(i=0; i<length; i++){
		crc = (unsigned char)(crc >>8) | (crc<<8);
		crc ^= this->messageString[i];
		crc ^= (unsigned char)(crc & 0xff) >> 4;
		crc ^= crc << 12;
		crc ^= (crc & 0x00ff) << 5;
    }

    if (crc == 0) // if it's 0x0000
    	return true;
    else return false;
}

LineInfo WLIP::parseLine(uint8_t lineNumber) {
	uint8_t lineByte = data.mDataHeader[lineNumber];
	LineInfo li;

	li.action = lineByte >> 6; // get the top 2 bits (MSB)
	li.length = lineByte & 0x3F; // get the first 6 bits

	return li;
}

uint8_t WLIP::getLine(uint8_t lineNumber) {
	return data.mDataHeader[lineNumber];
}

void WLIP::parsePayload() {
	// this->data.mPayload = this->message.mData.mPayload;
}

struct Line* WLIP::payloadToArray() {

	uint8_t pos = 0;
	static struct Line ll[10] = {{0}, {0}}; // FIXME: make it more dynamic (number of lines on the display)
	struct Line *lines = &ll[0];


	for (uint8_t i=0; i<header.dataHeaderLength; i++) {
		lines[i].info.action = parseLine(i).action;
		lines[i].info.length = parseLine(i).length;

		memset(lines[i].data, 0, 24); // line data is of size 32

		if (lines[i].info.length != 0) {
			for (int j=0; j<lines[i].info.length; j++) {
				lines[i].data[j] = data.mPayload[pos+j];
			}
		}
		lines[i].data[lines[i].info.length+1] = '\0';

		//char t[ll[i].info.length+1];
		// memcpy(t, &data.mPayload[pos], ll[i].info.length);
		/*for (int i=0; i<ll[i].info.length; i++) {
			t[i] = data.mPayload[pos+i];
		}*/
		// t[ll[i].info.length] = '\0';
		//ll[i].data = (char*)malloc(strlen(t)+1);
		// strcpy(ll[i].data, t);
		//strcpy(ll[i].data, "hey");

		pos += lines[i].info.length;
	}

	return ll;
}

/*char* WLIP::toString() {

	char buff[10];

	int LOC_MAXLEN = 255;
	// char *dest = (char*)malloc(LOC_MAXLEN*sizeof(char*));
	static char dest[255] = {0};
	memset(dest, 0, LOC_MAXLEN);

	snprintf(dest, LOC_MAXLEN, "%s", "Command: ");
	if (message.mCommand == 0xFF)
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "stream");
	else
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "unknown");

	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\nPacket Length: ");
	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa(header.packetLength, buff, 10));

	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\nSource ID: ");
	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa(header.sourceID, buff, 10));

	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\nData Control Mode: ");
	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa(header.dataControl.mode, buff, 10));

	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\nDH Length: ");
	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa(header.dataHeaderLength, buff, 10));

	for (uint8_t i=0; i<header.dataHeaderLength; i++) {
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\n  action: ");
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa((int)parseLine(i).action, buff, 10));
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\n  length: ");
		snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", itoa((int)parseLine(i).length, buff, 10));
	}

	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "\r\nData: ");
	snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s\n", data.mPayload);

	// snprintf(dest + strlen(dest), LOC_MAXLEN - strlen(dest), "%s", "lala");

	return dest;
}*/

char* WLIP::toStringTest() {

	const int BUFF_SIZE = 358;
	const int LINES_SIZE = 70;

	static char buffStr[BUFF_SIZE];
	char *buf = &buffStr[0];
	/*char *packetLength = itoa(header.packetLength, buff, 10);
	char *sourceID = itoa(header.sourceID, buff, 10);
	char *dataControlMode = itoa(header.dataControl.mode, buff, 10);
	char *dataHeaderLength = itoa(header.dataHeaderLength, buff, 10);*/
	// char *payload = (char*)malloc(127);
	// strcpy(payload, (const char*)data.mPayload);


	static char lines[LINES_SIZE];
	snprintf(lines, LINES_SIZE, "%s", "Line");
	for (uint8_t i=0; i<header.dataHeaderLength; i++) {
		snprintf(lines + strlen(lines), LINES_SIZE - strlen(lines), "%s%d:%d,%d", "\r\n", i, parseLine(i).action, parseLine(i).length);
		/*snprintf(lines + strlen(lines), 255 - strlen(lines), "%s", "\r\n  action: ");
		snprintf(lines + strlen(lines), 255 - strlen(lines), "%s", itoa((int)parseLine(i).action, buff, 10));
		snprintf(lines + strlen(lines), 255 - strlen(lines), "%s", "\r\n  length: ");
		snprintf(lines + strlen(lines), 255 - strlen(lines), "%s", itoa((int)parseLine(i).length, buff, 10));*/
	}

	snprintf(buf, BUFF_SIZE, "PacketLength: %d\r\n"
			"SourceID: %d\r\n"
			"DataControlMode: %d\r\n"
			"DataHeaderLength: %d\r\n"
			"%s\r\n"
			"Data: %s",
			header.packetLength, //packetLength,
			header.sourceID, //sourceID,
			header.dataControl.mode, //dataControlMode
			header.dataHeaderLength, //dataHeaderLength
			lines,
			(char*)data.mPayload
		);

	/*snprintf(buf, 255, "%s,%s,%s,%s",
				packetLength, //packetLength,
				sourceID, //sourceID,
				dataControlMode, //dataControlMode
				dataHeaderLength //dataHeaderLength
			);*/

	/*snprintf(buf, 255, "%s,%s,%s,%s",
			itoa(header.packetLength, buff, 10), //packetLength,
			itoa(header.sourceID, buff, 10), //sourceID,
			itoa(header.dataControl.mode, buff, 10), //dataControlMode
			itoa(header.dataHeaderLength, buff, 10) //dataHeaderLength
			);*/

	return buffStr;
}

