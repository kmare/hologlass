/*
 * WLIP.h
 *
 *  Created on: Sep 16, 2019
 *      Author: pioann
 */

#ifndef LIBS_PROTOCOLS_WLIP_WLIP_H_
#define LIBS_PROTOCOLS_WLIP_WLIP_H_

#include <avr/io.h>

#define HEADER_SIZE 3 // bytes for the header

enum CommandType {
	STREAM = 0xFF,
	QUERY = 0xFE,
	SLEEP = 0xFD,
	WAKEUP = 0xFC
};

enum DataMode {
	DEFAULT = 0x00,
	DOUBLE = 0x01,
	IMAGE = 0x02
};

enum DataHeaderAction {
	KEEP = 0x00,
	CLEAR = 0x01,
	CHANGE = 0x02
};

struct DataControl {
	uint8_t reserved;
	uint8_t mode;
};

struct Header {
	uint8_t packetLength; // 1 byte
	DataControl dataControl; // 1 byte
	uint8_t setupInfo; // 1 byte: 3 bits SourceID, 5 bits DataHeaderLength
	uint8_t sourceID; // 3 bits for the source ID
	uint8_t dataHeaderLength; // 5 bits for the length of the data header
};

// FIXME: apparently the problem is using pointers here. Try and make an implementation with them at some point
struct Data {
	uint8_t mDataHeader[20]; // size = rows * lineInfo (lineInfo is 1 byte)
	uint8_t mPayload[200]; // the actual data
	// uint8_t *mDataHeader; // size = rows * lineInfo (lineInfo is 1 byte)
	// uint8_t *mPayload; // the actual data
};

struct LineInfo {
	uint8_t action; // 2 bits (0..4) - 0: keep, 1: clear, 2: change
	uint8_t length; // 6 bits (0..63) - Line length
};

struct Line {
	LineInfo info;
	char data[24];
};

struct Message {
	uint8_t mCommand; // 1 byte
	uint8_t mHeader[HEADER_SIZE];
	Data mData;	 // 0..(rows*cols + rows) bytes // FIXME?
};


class WLIP {
private:
	uint8_t *messageString;
public:
	WLIP();
	~WLIP();

	Message message;
	Header header;
	Data data;

	void splitMessage();

	void build(uint8_t *m);

	void parseHeader();
	void parseDataHeader();
	LineInfo parseLine(uint8_t line);
	uint8_t getLine(uint8_t lineNumber);
	uint8_t getLineAction(uint8_t line);
	uint8_t getLineLength(uint8_t line);
	void parsePayload();
	struct Line* payloadToArray();
	bool isFrameValid(unsigned int length);
	bool isFrameValidTest(unsigned int length);

	const Message& getMessage() const;
	void setMessage(const Message &message);
	uint8_t* getMessageString() const;
	void setMessageString(uint8_t *messageString);

	char* toString();
	char* toStringTest();
};


#endif /* LIBS_PROTOCOLS_WLIP_WLIP_H_ */
