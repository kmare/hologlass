/*
 * Serial.h
 *
 *  Created on: Sep 10, 2019
 *      Author: pioann
 */

#ifndef LIBS_SERIAL_SERIAL_H_
#define LIBS_SERIAL_SERIAL_H_

#include <avr/io.h>

#ifndef F_CPU
#define F_CPU 16000000UL // uC frequency
#endif

#ifndef BAUD
#define BAUD 38400 // baud rate
#endif

#define BAUD_TOL 4 // just allow the interface to have 4% error

#include <util/setbaud.h> // helper for baud rate calculations

namespace wl {

// UART defines
#ifndef FOSC
#define	FOSC			16000000UL
#endif
#define TX_START()		UCSR0B |= _BV(TXEN0)	// Enable TX
#define TX_STOP()		UCSR0B &= ~_BV(TXEN0)	// Disable TX
#define RX_START()		UCSR0B |= _BV(RXEN0)	// Enable RX
#define RX_STOP()		UCSR0B &= ~_BV(RXEN0)	// Disable RX

#define RX_INTEN()		UCSR0B |= _BV(RXCIE0)	// Enable interrupt on RX complete
#define RX_INTDIS()		UCSR0B &= ~_BV(RXCIE0)	// Disable RX interrupt
#define TX_INTEN()		UCSR0B |= _BV(TXCIE0)	// Enable interrupt on TX complete
#define TX_INTDIS()		UCSR0B &= ~_BV(TXCIE0)	// Disable TX interrupt


class Serial {
public:
	Serial();
	~Serial();

	void initUART(void);
	void putByte(unsigned char data);
	void write(const char *s);
	void write(const char *str, char term);
	void writeln(const char *s);
	bool isDataAvailable();

	uint8_t getByte();
	uint8_t getByte(unsigned int timeout);
	uint8_t* getBytes(uint8_t until);
	uint8_t* getStream();
	uint8_t scan( char *buff, uint8_t bufferSize, unsigned int timeout);
	void flush();
	uint8_t getFrame( uint8_t *buff, uint8_t bufferSize, unsigned int timeout);
};

}

#endif /* LIBS_SERIAL_SERIAL_H_ */
