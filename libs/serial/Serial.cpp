/*
 * Serial.cpp
 *
 *  Created on: Sep 10, 2019
 *      Author: pioann
 */

#include "Serial.h"

#include <util/delay.h>
#include <string.h>

using namespace wl;

Serial::Serial() {

}

Serial::~Serial() {

}

// initialize UART
void Serial::initUART(void) {
	DDRD |= _BV(PD1);
	DDRD &= ~_BV(PD0);

	// Set baud rate; lower byte and top nibble
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;

// use double mode for 115200 baud rate
#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    // enable TX, RX
	TX_START();
	RX_START();

	// 8 bit, no parity, 1 bit stop. (only 1 or 2 bit stop configuration available on the atmega328)
	// http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf
	// page 161 - section 19.10.4
	UCSR0C = 0b00000110;

	RX_INTEN();
}

bool Serial::isDataAvailable() {
	// (UCSR0A & (1<<RXC0)) // should be the same
	return (UCSR0A & _BV(RXC0));
}


// send a byte
void Serial::putByte(unsigned char data) {
	// wait until data buffer is empty
	while (!(UCSR0A & _BV(UDRE0)));
	UDR0 = data; // put the data on UDR0
}

// send a string
void Serial::write(const char *str) {
	/*while (*str != '\0') {
		putByte(*str);
		++str;
	}*/
	while(*str) {
		putByte(*str++);
	}
	/*uint8_t i = 0;
	while ( str[i] )
    	putByte( str[i++] );*/
}

// send a string with termination character
void Serial::write(const char *str, char term) {
	while (*str != term) {
		putByte(*str);
		++str;
	}
}

void Serial::writeln(const char *str) {
	write((const char*)str);
	putByte('\r');
	putByte('\n');
}

uint8_t Serial::getByte() {
	while (!(UCSR0A & _BV(RXC0)));
	return UDR0;
}

uint8_t Serial::getByte(unsigned int timeout) {
	unsigned int i = 0;

	for ( i = 0; i < timeout; i++ ) {
		if ( UCSR0A & ( 1 << RXC0 ) )
			return UDR0;
		_delay_us( 1000000.0 / (double)BAUD * 10.0 );
	}
	return 0;
}


uint8_t* Serial::getBytes(uint8_t untilByte) {
	static uint8_t arr[100];
	memset(arr, 0, 100);
	uint8_t t = 0;

	uint8_t i = 0;
	while (i < 100 && t != untilByte) {
		while (!(UCSR0A & _BV(RXC0)));
		t = UDR0;
		arr[i] = t;
		i++;
	}

	return arr;
}

uint8_t* Serial::getStream() {
	static uint8_t arr[5];
	memset(arr, 0, 5);
	uint8_t i = 0;


	while (i<4) {
		while (  !(UCSR0A & _BV(RXC0)) );
		arr[i++] = UDR0;
	}

	/*while (!(UCSR0A & _BV(RXC0)));
	if (UDR0 == 0xFF) {
		arr[0] = 'h';
		arr[1] = 'e';
		arr[2] = 'l';
		arr[3] = 'l';
		arr[4] = 'o';
	}*/

	return arr;
}

uint8_t Serial::scan( char *buff, uint8_t bufferSize, unsigned int timeout) {
	memset(buff, 0, bufferSize);  // reset buffer
	unsigned int i = 0;

	while (i<bufferSize) { // TODO: also add a timeout
		while (!(UCSR0A & _BV(RXC0)));
		buff[i] = UDR0;
		if (buff[i] == 0x00) {
			return i;
		}
		++i;
	}

	return i;
}

void Serial::flush() {
	unsigned char dummy;
	while ( UCSR0A & (1<<RXC0) ) {
		dummy = UDR0;
	}
}


uint8_t Serial::getFrame( uint8_t *buff, uint8_t bufferSize, unsigned int timeout) {
	// memset(buff, 0, 100);  // reset buffer
	unsigned int i = 0;

	while (i<4) {
		while (!(UCSR0A & _BV(RXC0)));
		buff[i] = UDR0;
		i++;
		// putByte(buff[i]);
	}

	return 0;
}
