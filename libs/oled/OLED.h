/*
 * OLED.h
 *
 *  Created on: Aug 20, 2019
 *      Author: pioann
 */

#ifndef LIBS_OLED_OLED_H_
#define LIBS_OLED_OLED_H_

#include <avr/io.h>
#include <inttypes.h>
#include <avr/pgmspace.h>

#define DISPLAY_OFF        0xAE
#define DISPLAY_ON        0xAF

#define WHITE            0x01
#define BLACK            0x00

#define OLED_I2C_ADR         (0x78 >> 1)

#define FONT ssd1306oled_font

#define NORMALSIZE 1
#define DOUBLESIZE 2


enum MODE {
	text = 0,
	graphics = 1
};

struct curPos {
    uint8_t x;
    uint8_t y;
};

class OLED {
private:
	uint8_t width = 128;
	uint8_t height = 64;
	uint8_t charMode = NORMALSIZE;

	curPos cursorPosition;

	void command(uint8_t cmd[], uint8_t size);
	void data(uint8_t data[], uint16_t size);

public:
	void gotoxy(uint8_t x, uint8_t y);
	void clrScr(void);
	void clrRow(uint8_t row);
	void home(void);
	void writeChar(char c);
	void write(const char* s);
	uint8_t getMode() const;
	void setMode(uint8_t mode);
	
public:
	OLED();
	~OLED();

	void init();
};

#endif /* LIBS_OLED_OLED_H_ */
