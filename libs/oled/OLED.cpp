/*
 * OLED.cpp
 *
 *  Created on: Aug 20, 2019
 *      Author: pioann
 */

#include "OLED.h"
#include "i2c.h"
#include "font.h"

#include <string.h>

OLED::OLED() {

}

OLED::~OLED() {

}

const uint8_t PROGMEM init_sequence []  = {    // Initialization Sequence
	DISPLAY_OFF,    // Display OFF (sleep mode)
	0x20, 0b00,        // Set Memory Addressing Mode
	// 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
	// 10=Page Addressing Mode (RESET); 11=Invalid
	0xB0,            // Set Page Start Address for Page Addressing Mode, 0-7
	0xC8,            // Set COM Output Scan Direction
	0x00,            // --set low column address
	0x10,            // --set high column address
	0x40,            // --set start line address
	0x81, 0x3F,        // Set contrast control register
	0xA1,            // Set Segment Re-map. A0=address mapped; A1=address 127 mapped.
	0xA6,            // Set display mode. A6=Normal; A7=Inverse
	0xA8, 0x3F,        // Set multiplex ratio(1 to 64)
	0xA4,            // Output RAM to Display
	// 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
	0xD3, 0x00,        // Set display offset. 00 = no offset
	0xD5,            // --set display clock divide ratio/oscillator frequency
	0xF0,            // --set divide ratio
	0xD9, 0x22,        // Set pre-charge period
	0xDA, 0x12,        // Set com pins hardware configuration
	0xDB,            // --set vcomh
	0x20,            // 0x20,0.77xVcc
	0x8D, 0x14,        // Set DC-DC enable
};

void OLED::command(uint8_t cmd[], uint8_t size) {
    i2c_start((OLED_I2C_ADR << 1) | 0);
    i2c_byte(0x00);    // 0x00 for command, 0x40 for data
    for (uint8_t i=0; i<size; i++) {
        i2c_byte(cmd[i]);
    }
    i2c_stop();
}

void OLED::data(uint8_t data[], uint16_t size) {
    i2c_start((OLED_I2C_ADR << 1) | 0);
    i2c_byte(0x40);    // 0x00 for command, 0x40 for data
    for (uint16_t i = 0; i<size; i++) {
        i2c_byte(data[i]);
    }
    i2c_stop();
}

void OLED::gotoxy(uint8_t x, uint8_t y) {
    if( x > (OLED::width/sizeof(FONT[0])) || y > (OLED::height/8-1))
    	return;// out of display
    x = x * sizeof(FONT[0]);
    cursorPosition.x = x;
    cursorPosition.y = y;

    uint8_t commandSequence[] = {
    		(uint8_t)(0xb0+y),
			0x21,
			(uint8_t)(0x00+((2+x) & (0x0f))),
			(uint8_t)(0x10+( ((2+x) & (0xf0)) >> 4) ),
			0x7f
    };

    OLED::command(commandSequence, sizeof(commandSequence));
}

uint8_t OLED::getMode() const {
	return this->charMode;
}

void OLED::setMode(uint8_t mode) {
	this->charMode = mode;
}

void OLED::init() {
	i2c_init();

	uint8_t commandSequence[sizeof(init_sequence)+1];

	for (uint8_t i = 0; i < sizeof (init_sequence); i++) {
		commandSequence[i] = (pgm_read_byte(&init_sequence[i]));
	}

	commandSequence[sizeof(init_sequence)] = DISPLAY_ON;
	OLED::command(commandSequence, sizeof(commandSequence));
	clrScr();
}

void OLED::clrScr(void) {
    uint8_t displayBuffer[OLED::width];
    memset(displayBuffer, 0x00, sizeof(displayBuffer));
    for (uint8_t i = 0; i < OLED::height/8; i++) {
        gotoxy(0,i);
        data(displayBuffer, sizeof(displayBuffer));
    }

    OLED::home();
}

void OLED::clrRow(uint8_t row) {
    uint8_t displayBuffer[OLED::width];
    memset(displayBuffer, 0x00, sizeof(displayBuffer));
	gotoxy(0, row);
	data(displayBuffer, sizeof(displayBuffer));
	if (charMode == DOUBLESIZE) {
		gotoxy(0, row+1);
		data(displayBuffer, sizeof(displayBuffer));
	}
}

void OLED::home(void) {
    gotoxy(0, 0);
}

void OLED::writeChar(char c) {
    switch (c) {
        case '\b':
            // backspace
            gotoxy(cursorPosition.x-charMode, cursorPosition.y);
            writeChar(' ');
            gotoxy(cursorPosition.x-charMode, cursorPosition.y);
            break;
        case '\t':
            // tab
            if ( (int8_t)(cursorPosition.x+charMode*4) < (int8_t)(OLED::width / sizeof(FONT[0])-charMode*4) ) {
                gotoxy(cursorPosition.x+charMode*4, cursorPosition.y);
            } else {
                gotoxy(OLED::width/ sizeof(FONT[0]), cursorPosition.y);
            }
            break;
        case '\n':
            // new line
            if(cursorPosition.y < (OLED::width/8-1)) {
                gotoxy(cursorPosition.x, cursorPosition.y+charMode);
            }
            break;
        case '\r':
            // carriage return
            gotoxy(0, cursorPosition.y);
            break;
        default:
            // char doesn't fit in line
            if( (cursorPosition.x >= OLED::width-sizeof(FONT[0])) || (c < ' ') )
            	break;
            // mapping char
            c -= ' ';
            if (c >= pgm_read_byte(&special_char[0][1]) ) {
                char temp = c;
                c = 0xff;
                for (uint8_t i=0; pgm_read_byte(&special_char[i][1]) != 0xff; i++) {
                    if ( pgm_read_byte(&special_char[i][0])-' ' == temp ) {
                        c = pgm_read_byte(&special_char[i][1]);
                        break;
                    }
                }
                if ( c == 0xff ) break;
            }
            // print char at display

            if (charMode == DOUBLESIZE) {
                uint16_t doubleChar[sizeof(FONT[0])];
                uint8_t dChar;

                for (uint8_t i=0; i < sizeof(FONT[0]); i++) {
                    doubleChar[i] = 0;
                    dChar = pgm_read_byte(&(FONT[(uint8_t)c][i]));
                    for (uint8_t j=0; j<8; j++) {
                        if ((dChar & (1 << j))) {
                            doubleChar[i] |= (1 << (j*2));
                            doubleChar[i] |= (1 << ((j*2)+1));
                        }
                    }
                }
                i2c_start(OLED_I2C_ADR << 1);
                i2c_byte(0x40);
                for (uint8_t i = 0; i < sizeof(FONT[0]); i++) {
                    // print font to ram, print 6 columns
                    i2c_byte(doubleChar[i] & 0xff);
                    i2c_byte(doubleChar[i] & 0xff);
                }
                i2c_stop();

				// defined SH1106
                uint8_t commandSequence[] = {
                		(uint8_t)(0xb0+cursorPosition.y+1),
						0x21,
						(uint8_t)(0x00 + ((2+cursorPosition.x) & (0x0f))),
						(uint8_t)(0x10 + ( ((2+cursorPosition.x) & (0xf0)) >> 4 )),
						0x7f
                };

                command(commandSequence, sizeof(commandSequence));

                i2c_start(OLED_I2C_ADR << 1);
                i2c_byte(0x40);
                for (uint8_t j = 0; j < sizeof(FONT[0]); j++) {
                    // print font to ram, print 6 columns
                    i2c_byte(doubleChar[j] >> 8);
                    i2c_byte(doubleChar[j] >> 8);
                }
                i2c_stop();

                commandSequence[0] = 0xb0+cursorPosition.y;

				// defined SH1106
                commandSequence[2] = 0x00+((2+cursorPosition.x+(2*sizeof(FONT[0]))) & (0x0f));
                commandSequence[3] = 0x10+( ((2+cursorPosition.x+(2*sizeof(FONT[0]))) & (0xf0)) >> 4 );

                command(commandSequence, sizeof(commandSequence));
                cursorPosition.x += sizeof(FONT[0])*2;
            } else {
                i2c_start(OLED_I2C_ADR << 1);
                i2c_byte(0x40);
                for (uint8_t i = 0; i < sizeof(FONT[0]); i++) {
                    // print font to ram, print 6 columns
                    i2c_byte(pgm_read_byte(&(FONT[(uint8_t)c][i])));
                }
                i2c_stop();
                cursorPosition.x += sizeof(FONT[0]);
            }

            break;
    }
}

void OLED::write(const char* s) {
    while (*s) {
    	writeChar(*s++);
    }
}
