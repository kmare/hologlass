/*
 * COBS.h
 *
 *  Created on: Oct 1, 2019
 *      Author: pioann
 */

#ifndef LIBS_COBS_COBS_H_
#define LIBS_COBS_COBS_H_

#include <avr/io.h>

#define SIZE(Data) (sizeof(Data)/sizeof(Data[0]))

#define StartBlock()	(code_ptr = dst++, code = 1)
#define FinishBlock()	(*code_ptr = code)


class COBS {
public:
	COBS();
	~COBS();

	static uint8_t encode(const uint8_t *ptr, uint16_t length, uint8_t *dst);
	static uint8_t decode(const uint8_t *ptr, uint16_t length, uint8_t *dst);
};

#endif /* LIBS_COBS_COBS_H_ */
