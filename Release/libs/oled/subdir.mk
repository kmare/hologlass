################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libs/oled/font.c \
../libs/oled/i2c.c 

CPP_SRCS += \
../libs/oled/OLED.cpp 

C_DEPS += \
./libs/oled/font.d \
./libs/oled/i2c.d 

OBJS += \
./libs/oled/OLED.o \
./libs/oled/font.o \
./libs/oled/i2c.o 

CPP_DEPS += \
./libs/oled/OLED.d 


# Each subdirectory must supply rules for building sources it contributes
libs/oled/%.o: ../libs/oled/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

libs/oled/%.o: ../libs/oled/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


