################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../libs/protocols/wlip/WLIP.cpp 

OBJS += \
./libs/protocols/wlip/WLIP.o 

CPP_DEPS += \
./libs/protocols/wlip/WLIP.d 


# Each subdirectory must supply rules for building sources it contributes
libs/protocols/wlip/%.o: ../libs/protocols/wlip/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


