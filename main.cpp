/*
 * main.cpp
 *
 *  Created on: Aug 20, 2019
 *      Author: pioann
 */

#include "libs/serial/Serial.h"
#include "libs/protocols/wlip/WLIP.h"
#include "libs/cobs/COBS.h"
#include "libs/oled/OLED.h"

#include <util/delay.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
uint8_t i = 0;
uint8_t prev_i = i;

void scrollText(OLED *oled) {
	// oled.clrScr();
	oled->clrRow(prev_i);
	oled->gotoxy(0, i);
	if (i % 2)
		oled->setMode(NORMALSIZE);
	else
		oled->setMode(DOUBLESIZE);
	oled->write("weirdloop!");
	prev_i = i;
	i++;
	if (i > 6)
		i = 0;
}

void showTimer(OLED *oled) {
	oled->clrRow(4);
	oled->gotoxy(2, 4);
	char s[3];
	oled->write(itoa(i, s, 10));
	i++;
	if (i > 59)
		i = 0;
}

void testOver(OLED *oled) {
	oled->gotoxy(2, 4);
	oled->write("smeeagain");
	oled->gotoxy(2, 4);
	oled->write("hello");
}
*/

int main(void){

	char buff[255] = {0};
	char intBuff[10];

	wl::Serial ser;
	ser.initUART();

	WLIP msg;

	OLED oled;
	oled.init();

	oled.gotoxy(2, 2);
	oled.write("booting");
	ser.writeln("booting");
	_delay_ms(200);
	oled.write(".");
	_delay_ms(200);
	oled.write(".");
	_delay_ms(200);
	oled.write(".");
	_delay_ms(200);
	oled.clrScr();
	oled.write("waiting for packets");
	ser.writeln("waiting for packets");

	while(1) {

		while(!ser.isDataAvailable()) {
			continue;
		}

		uint8_t fsize = ser.scan(buff, 255, 255);

		// ser.write("Frame size: ");
		// ser.writeln(itoa(fsize, intBuff, 10));

		unsigned char dec[255] = {0};
		uint8_t decSize = COBS::decode((unsigned char*)buff, fsize, dec);

		// check if the frame is actually the right size (including the COBS '0' and the command byte)
		if ((fsize-2) == dec[1]) {

			ser.write("DecSize: ");
			ser.writeln(itoa(decSize, intBuff, 10));

			// oled.clrScr();
			uint8_t m = oled.getMode();

			msg.build(dec);
			// ser.writeln(itoa(msg.isFrameValidTest(decSize), intBuff, 10)); // CRC

			if (msg.isFrameValidTest(decSize)) {
				ser.writeln("valid frame");
			} else {
				ser.writeln("frame is invalid");
				oled.clrScr();
				oled.gotoxy(2, 2);
				oled.write("Invalid frame...");
				continue;
			}

			ser.writeln(msg.toStringTest());

			switch(msg.message.mCommand) {
			case 0xFF:
				ser.writeln("processing stream");
				break;
			case 0xFE:
				ser.writeln("processing query");
				break;
			default:
				ser.writeln("unknown command");
			}

			// oled.setMode(NORMALSIZE);
			if (msg.header.dataControl.mode == 0) {
				ser.writeln("normal mode");
				oled.setMode(NORMALSIZE);
			}
			if (msg.header.dataControl.mode == 1) {
				ser.writeln("double mode");
				oled.setMode(DOUBLESIZE);
			}

			/*if (msg.message.mHeader[1] == (0x02 & 0x0F)) {
				ser.writeln("image mode: unsupported at the moment");
				oled.setMode(NORMALSIZE);
			}*/

			// reset the screen when switching modes (normal <-> double)
			if (m != oled.getMode())
				oled.clrScr();

			Line *lines = msg.payloadToArray();
			for (uint8_t i=0; i<msg.header.dataHeaderLength; i++) {
				ser.writeln(lines[i].data);
				if (lines[i].info.action == 2) {
					oled.clrRow(i * oled.getMode());
					oled.gotoxy(0, i * oled.getMode());
					oled.write(lines[i].data);
				}
				if (lines[i].info.action == 1) {
					oled.clrRow(i * oled.getMode());
				}
				if (lines[i].info.action == 0) {

				}
			}
			free(lines);
		} else {
			oled.clrScr();
			oled.write("invalid frame");
			ser.writeln("invalid frame :(");
			ser.writeln((char*)dec);
		}

		ser.flush();

		// memset(dec, 0, 255);
		// memset(buff, 0, 255);
	}

	return 0;
}

void test() {

	//oled.clrScr();
	//oled.gotoxy(2, 4);
	//oled.write("all good!");

	/*oled.clrRow(prev_i);
	oled.gotoxy(0, i);
	if (i % 2)
		oled.setMode(NORMALSIZE);
	else
		oled.setMode(DOUBLESIZE);
	oled.write("weirdloop!");
	prev_i = i;
	i++;
	if (i > 6)
		i = 0;*/
	// scrollText(&oled);
	// showTimer(&oled);
	// testOver(&oled);

	// _delay_ms(100);
}
